FROM ubuntu:latest
MAINTAINER Michael Dodwell <michael@dodwell.us>

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update -y && \ 
    apt-get install -y \
    software-properties-common \
    curl \
    wget \
    apt-transport-https

# Setup oracle and artifactory deb locations
RUN echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | debconf-set-selections && \
    curl https://bintray.com/user/downloadSubjectPublicKey?username=jfrog | apt-key add - && \
    echo 'deb https://jfrog.bintray.com/artifactory-debs xenial main' > /etc/apt/sources.list.d/nginx.list && \
    add-apt-repository -y ppa:webupd8team/java


RUN apt-get update -y && \ 
    apt-get install -y \
    oracle-java8-installer \
    oracle-java8-set-default \
    jfrog-artifactory-oss && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* \
           /tmp/* \
           /var/tmp/*

RUN mkdir -p /etc/opt/jfrog/artifactory /var/opt/jfrog/artifactory/{data,logs,backup} && \
    chown -R artifactory: /etc/opt/jfrog/artifactory /var/opt/jfrog/artifactory/{data,logs,backup} && \
    mkdir -p /var/opt/jfrog/artifactory/defaults/etc && \
    cp -rp /etc/opt/jfrog/artifactory/* /var/opt/jfrog/artifactory/defaults/etc/

ENV ARTIFACTORY_HOME /var/opt/jfrog/artifactory

# Define commonly used JAVA_HOME variable
ENV JAVA_HOME /usr/lib/jvm/java-8-oracle

EXPOSE 8081

ADD build/start.sh /start.sh
RUN chmod 755 /start.sh

CMD ["/start.sh"]
